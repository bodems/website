+++
title = "Tag des offenen Hackspace"
date = 2022-08-27 14:00:00
+++

Zum Tag des offenen Hackspace laden wir alle Interessierten ein, unsere
Räume zu besichtigen und über Projekte zu quatschen. Es werden natürlich
Sticker und Buttons zum mitnehmen angeboten. Außerdem haben wir Lötsets
vorrätig, falls jemand löten lernen möchte.
