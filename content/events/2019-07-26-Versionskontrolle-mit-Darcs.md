+++
title = "Versionskontrolle mit Darcs"
date = 2019-07-26 20:00:00
+++

[Darcs](https://darcsbook.acmelabs.space) ist ein freundliches
[Versionskontrollsystem](https://de.wikipedia.org/wiki/Versionsverwaltung) das
uns dabei unterstützt Veränderungen an z.B. Textdateien zu verwalten. Dieser
Vortrag soll als Einstieg in das Thema Versionskontrolle mit `darcs` dienen.
Vorkenntnisse sind nicht erforderlich.
