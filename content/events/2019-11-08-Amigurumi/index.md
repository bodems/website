+++
title = "Amigurumi Workshop"
date = 2019-11-08 19:30:00
+++

![Einige Amigurumi Stofftiere](amigurumi.jpeg)\

Amigurumi ist eine aus Japan stammende Häkelkunst, mit der sich kleine,
stofftierartige Figuren oder Gegenstände gestalten lassen. Monika möchte
euch in einem Kurzen Workshop die Grundlagen und Möglichkeiten zeigen, die
diese Technik bietet.

<!-- more -->

Wir haben genug Wolle für die Dauer des Workshops hier, und werden auch ein
paar Häkelnadeln da haben. Falls ihr gerne selbst was mitbringen möchtet, wäre
eine Häkelnadel der Göße 3,0 oder 3,5 sowie Wolle aus 100% Baumwolle mit einer
Lauflänge von c.a. 125m/50g optimal.

### Literaturliste:

**Lesezeichen:**\
Matthies, Jonas: Bookies: tierische Lesezeichen zum Häkeln\
ISBN 978-3-7724-8127-7

**Wohnwagen:**\
Happy Holiday häkeln: häkel dir dein Abenteuer!\
ISBN 978-3-7724-6442-3

**Enten:**\
Behn, Carola: Quietsche-Entchen häkeln: Ente gut - alles gut\
ISBN 978-3-7724-6974-9

**Schlüsselanhänger:**\
Konrad, Esther: Schlüsseltiere häkeln: kleine Freunde für Unterwegs\
ISBN 978-3-7724-6992-3

**Emojis:**\
Konrad, Esther: Emoji häkeln: Gehäkelte Emojis für jede Stimmungslage\
ISBN 978-3-7724-6466-9

**Geschirr, Gebäck, Obst und Kakteen:**\
Häkeln to go: Häkelminis, einfach und bezaubernd\
ISBN 978-3-7724-6430-0

Süße Häkelfiguren: Putzige Tierchen und Leckereien im Miniformat\
ISBN 978-3-625-17068-6

Die 100 schönsten Häkelideen\
ISBN 978-3-7724-6347-1

**Oktopus und Maus:**\
Süße Häkelfiguren: Putzige Tierchen und Leckereien im Miniformat\
ISBN 978-3-625-17068-6

**Biene und Apfel mit Wurm:**\
Tierisch süße Häkelfreunde 4: Putzige Amigurumis häkeln\
ISBN 978-3-86355-654-9
