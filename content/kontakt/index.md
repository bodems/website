+++
title = "Kontakt"
pagetitle = "Kontakt"
+++

Wir sind auf [Matrix](https://matrix.org/) unter folgender Adresse zu erreichen.

{% address(center=true) %}
[`#layer8:acmelabs.space`](https://matrix.to/#/#layer8:acmelabs.space)
{% end %}

Oder über unseren Space:

{% address(center=true) %}
[`#acmespace:acmelabs.space`](https://matrix.to/#/#acmespace:acmelabs.space)
{% end %}

Falls du dich nicht mit Matrix auskennst haben wir eine [kleine
Anleitung](@/projekte/matrix-howto/index.md) für dich vorbereitet.

Für offizielle Anfragen oder falls du im Chat niemanden erreichst, schreib gerne
auch eine Mail an den Vorstand unter:

{% address(center=true) %}
[`vorstand@acmelabs.space`](mailto:vorstand@acmelabs.space)
{% end %}

Wenn du über Veranstaltungen und andere Neuigkeiten informiert werden möchtest,
kannst du dich auf unserer Mailing List anmelden. Hier zu musst du eine Mail an
[`announce+subscribe@acmelabs.space`](mailto:announce+subscribe@acmelabs.space)
senden. Der Betreff und Inhalt sind egal.

Wenn du dich von der Mailing Liste wieder abmelden willst musst du nur eine Mail
an
[`announce+unsubscribe@acmelabs.space`](mailto:announce+unsubscribe@acmelabs.space)
senden.

Diese Mailing Liste ist read-only. Das heißt, dass du nur Mails empfangen kannst
aber keine senden kannst.

Unsere Räume befinden sich in der Mühlenstraße 5 in Bielefeld. Am besten fragst
du aber vorher nach, wann jemand da ist, der dich rein lassen kann, da wir keine
festen Öffnungszeiten haben. Du kannst aber davon ausgehen, dass wir am
wahrscheinlichsten am Donnerstag oder Freitag vor Ort sein werden.

[![Abbildung einer Karte auf der die Position der ACME Labs e.V verzeichnet
ist](osm_location.png)](https://www.openstreetmap.org/node/2720832123#map=18/52.02039/8.54724)\

<div class="copyright">
© OpenStreetMap contributors
</div>
