+++
title = "Kranich Mobile"
description = "1.000 Origami Kraniche in einer 10x10x10 Anordnung mit LED Beleuchtung."
[extra]
url = "https://chaos.social/tags/AllCranesAreBeautiful"
responsible = "tauli et al."
status = "Done"
image = "kranich_mobile.jpeg"
imageAlt = "An installation of rainbow coloured paper cranes suspended in the shape of a cube."
+++