+++
title = "Solarbox"
description = "Eine halb-mobile Solarbox mit insgesamt 150W PV-Modulen, MPPT-Laderegler, 14Ah Bleibatterie" und 12V-KFZ-Buchsen für Geräte. Der Laderegler wird über einen BananaPi M2 Zero ausgelesen und stellt die Werte auf einer Webseite dar."
[extra]
url = "https://codeberg.org/bodems/Solar-Monitoring/"
responsible = "bodems"
status = "done und wächst"
image = "../default-images/project-soldering-iron.jpg"
imageAlt = "A soldering iron on a blue background."
+++
