+++
title = "Ein kurzes ACME-Labs Matrix-HowTo"
description = "Anleitung für die Nutzung des Matrix Server"
[extra]
responsible = "λTotoro"
image = "../default-images/project-pc-writing.jpg"
imageAlt = "A desktop pc with writing on the monitor on an orange background."
status = "Abgeschlossen"
+++

## Ein kurzes ACME-Labs Matrix-HowTo

### Was ist Matrix und warum Matrix?

[Matrix](https://de.wikipedia.org/wiki/Matrix_(Kommunikationsprotokoll))
ist ein offenes Kommunikationsprotokoll, dass für verschiedene Zwecke (wie z.B.
Chats oder Internettelefonie) verwendet werden kann. Wir bei ACME Labs haben uns
entschieden, unseren öffentlichen Gruppenchat über Matrix laufen zu lassen, weil
es im Vergleich mit anderen Platformen gewisse Vorteile bietet. Dieses Tutorial
erklärt kurz, wie auch du daran teilhaben kannst!

### Wie kann ich dem Gruppenchat beitreten?

Der empfohlene Weg für Menschen, die sich mit dem Protokoll noch nicht genau auskennen ist der 
[element.io](https://element.io/) Client. Den gibt es in verschiedenen Ausführungen. Es existiert ein 
[Webinterface](https://element.io/get-started) für alle modernen Browser, aber auch Apps für Apple iOS
([Link zum AppStore](https://apps.apple.com/us/app/vector-im/id1083446067)) und Android (verfügbar sowohl durch
[Google Play](https://play.google.com/store/apps/details?id=im.vector.app) als auch
[F-Droid](https://f-droid.org/en/packages/im.vector.app/), was auch immer du benutzt).

Die Screenshots in diesem Tutorial sind vom Webinterface (Stand: 2021-10-24), aber der Ablauf ist überall ähnlich.

#### Schritt 1: Account anlegen

Als allererstes wirst du dir einen Account anlegen müssen. Diese Anleitung
orientiert sich am [Webinterface](https://element.io/get-started).
Wir gehen alle wichtigen Einstellungen durch, wenn hier etwas nicht erwähnt
wird, ist es okay, es einfach auf der vorgegebenen Einstellung zu lassen.

Um loszulegen klickst du dafür einfach auf den grünen "Element Web starten"
Button und danach auf den grünen "Konto erstellen" Button.

Auf der nächsten Seite wirst du nach einem Namen, einem Passwort (was
du zwei Mal eingeben musst) und einer E-Mail-Adresse gefragt. Name wie Passwort
kannst du frei wählen, achte aber besser darauf, kein zu schwaches Passwort (wie
z.B. den Namen deines Haustiers gefolgt vom Geburtsjahr, "passwort123456" oder
"hunter2") zu benutzen.

Die E-Mail-Adresse, die du angibst, wird verwendet um deinen
Account zurück zu bekommen falls du dein Passwort verlieren oder vergessen
solltest. Damit klar ist, dass es auch wirklich deine E-Mail-Adresse ist, wird
dir nach der Registrierung eine Mail geschickt mit einem Link, den du klicken
kannst, um das zu bestätigen.

Alternativ kannst du dir auch einen Account anlegen, der mit einem
Account auf einem anderen Service verbunden ist, wie zum Beispiel GitHub,
GitLab, Google, Facebook oder Apple. Um dies zu tun, klicke auf das
entsprechende Symbol und folge den Anweisungen auf dem Bildschirm (wir gehen
hier nicht alle diese Einzelfälle durch).

![Screenshot einer Anmeldung auf dem element Webinterface](matrix_howto_1.png)

Wenn du alles zu deiner Zufriedenheit eingegeben hast, klickst du auf
"Registrieren". Auf der nächsten Seite wirst du gebeten, ein reCAPTCHA zu
bestehen (das ist nur eine Box, die du anklicken musst. Sie überprüft im
Hintergrund, ob sich gerade wirklich ein Mensch anmelden möchte. Das verhindert,
dass sich jemand automatisiert viel zu viele Accounts anlegt) und die
Nutzungsbedingungen von Matrix zu akzeptieren.

#### Schritt 2: Anmelden

Sobald dieser Prozess abgeschlossen ist kannst du dich wie bei anderen Websites
auch im [Webinterface](https://element.io/get-started) anmelden. Klicke auf den
blauen "Anmelden"-Knopf und gib den Namen und das Passwort, die du eben für
deinen Account festgelegt hast. Noch ein Klick auf den grünen "Anmelden"-Knopf
und du solltest sehen, wie sich die Oberfläche ändert, mit deinem Accountnamen
oben links (hier klicken, um die Einstellungen für deinen Account zu ändern oder
dich wieder abzumelden) und einem hellgrauen Streifen am linken Bildschirmrand.

#### Schritt 3: Raum beitreten

Fast geschafft! Du siehst jetzt die Oberfläche für eingeloggte Accounts. Unter
"Personen" findest du Chats mit einzelnen Personen und unter "Räume" findest du
Gruppenchats. Momentan sind allerdings beide Kategorien bei dir wahrscheinlich
noch leer. Das wollen wir jetzt ändern. Klicke dafür oben links auf "Räume
erkunden" (mit dem kleinen Kompass als Symbol) und gib in der Suchleiste des
Fensters, das aufspringt folgenden Text ein:

::::: {.sauerkirsche}
`#layer8:acmelabs.space`
:::::

Das ist der Name unseres Gruppenchats. Wenn du ihn eingegeben hast, klicke auf
"Beitreten" oder drücke einfach Return. Es kann sein, dass die Suchfunktion erst
sagt, dass sie den Raum nicht gefunden hat, aber wenn du auf "Beitreten"
klickst, solltest du trotzdem rein kommen.

![Screenshot einer Raumsuche im element Webinterface, der layer8 Raum von
acmelabs.space ist angewählt](matrix_howto_2.png)

#### Schritt 4: Chatten!

Das war's! Jetzt bist du mit unserem Gruppenchat verbunden und kannst mit allen
anderen Teilnehmenden dort reden. Sag' doch zum Beispiel einfach Mal "Hallo!"
und erzähle uns, was dich interessiert und wie du zu uns gefunden hast!

### Geht nicht? Alles kaputt? Sitzt du fest?

Wenn du mit irgendeinem der hier beschriebenen Schritte Probleme hast oder aus
einem anderen Grund auf dem Weg zum Gruppenchat fest sitzt, kannst du uns unter
[`admin@acmelabs.space`](mailto:admin@acmelabs.space) eine E-Mail schreiben, in
der du das Problem schilderst. Wir können nichts versprechen, aber wir werden
auf jeden Fall versuchen, dir fix weiter zu helfen.
