+++
title = "Vault TV"
description = "Telefunken Bilderrahmen in einem tatsächlichen Bilderrahmen mit Deko Elementen, damit das Case nach einem kleinen 60s Fernseher aussieht und zeigt, was man im Lab tun kann."
[extra]
responsible = "Kascha"
status = "Done"
hint = "[Bulk Image Convert](https://github.com/Kaisa-Marysia/VaultTV/blob/main/Telefunken-Imageframe.sh)"
image = "vault-tv.jpg"
imageAlt = "A digital picture frame. The screen reads 'A collective making everything'."
+++