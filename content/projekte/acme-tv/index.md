+++
title = "ACME TV (Smart TV)"
description = "TFT Display im Vintage TV Gehäuse welches auf Retroterm Grafiken anzeigt, das aktuelle Wetter anzeigt und die Abfahrtpläne vom Bielefelder Bahnhof."
[extra]
responsible = "Kascha"
status = "Done und wächst"
hint = "Kann einfach in einem [Shellscript](https://github.com/Kaisa-Marysia/VaultTV/blob/main/script.sh) erweitert werden."
image = "acme-tv.jpg"
imageAlt = "A retro tv with the acme labs logo next to a small lamp."
+++