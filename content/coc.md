+++
title = "Code of Conduct"
+++

### Kurzfassung

ACME Labs hat es sich zum Ziel gemacht, einen diskriminierungsfreien
Raum für alle Menschen zu schaffen, unabhängig von Geschlecht,
Geschlechtsidentität, sexueller Orientierung, Behinderungen, äußerer
Erscheinung, Körperbau, Alter, Hautfarbe, ethnische Zugehörigkeit oder
Religion. Wir tolerieren in keiner Weise die Belästigung oder
Diskriminierung von Gästen oder Mitgliedern.

Dieser Code of Conduct gilt für alle Räume von ACME Labs also für die
physischen Vereinsräume aber auch Online-Präsenzen wie Mailinglisten,
Chat, Wiki, etc. Verstöße gegen diesen Code of Conduct können zur
Folge haben, dass der Vorstand Rechte einschränkt oder die betreffende
Person von einzelnen Bereichen des Vereins oder ganz aus dem Verein
ausschließt.

Einige Bereiche von ACME Labs können zusätzlichen Regeln unterliegen,
welche den Teilnehmer\*innen offen zugänglich gemacht werden. Es
unterliegt der Verantwortung derer, die sich in diesen Räumen bewegen,
sich über zusätzliche Regeln zu informieren und diese zu befolgen.

### Längere Version

ACME Labs hat es sich zum Ziel gemacht, einen diskriminierungsfreien
Raum für alle Menschen zu schaffen. Wir tolerieren in keiner Weise die
Belästigung oder Diskriminierung von Gästen oder Mitgliedern.

Dieser Code of Conduct gilt für alle Räume von ACME Labs, also für die
physischen Vereinsräume aber auch Online-Präsenzen wie Mailinglisten,
Chat, Wiki, etc. Verstöße gegen diesen Code of Conduct können zur
Folge haben, dass der Vorstand Rechte einschränkt oder die betreffende
Person von einzelnen Bereichen des Vereins oder ganz aus dem Verein
ausschließt.

Einige Bereiche von ACME Labs können zusätzlichen Regeln unterliegen,
welche den Teilnehmer\*innen offen zugänglich gemacht werden. Es
unterliegt der Verantwortung derer, die sich in diesen Räumen bewegen,
sich über zusätzliche Regeln zu informieren und diese zu befolgen.

Als Belästigung werten wir unter anderem:

-   Verletzende Bemerkungen in Bezug auf Geschlecht,
    Geschlechtsidentität, sexuelle Orientierung, Behinderungen,
    psychische Erkrankungen, Neuro(a)typizität, äußere Erscheinung,
    Körperbau, Alter, Hautfarbe, ethnische Zugehörigkeit oder Religion.
-   Unerwünschte Bemerkungen bezüglich der Lebensweise einer Person,
    z.B. in Bezug auf Ernährung, Gesundheit, Kindererziehung, Drogen
    oder Arbeitsverhältnis.
-   Absichtliches misgendering oder die Benutzung von „toten“ oder
    abgelegten Namen.
-   Unnötige oder off-topic pornografische Bilder oder sexuelles
    Verhalten in Bereichen, wo dies nicht angemessen ist.
-   Physisches Berühren oder simuliertes Berühren (z.B. schriftliche
    Beschreibungen wie „\*hug\*“) ohne Einverständnis oder nach
    Aufforderung, dies zu unterlassen.
-   Androhung von Gewalt.
-   Anstiftung zur Gewalt gegen eine Person, einschließlich des
    Ermunterns einer Person zu Selbstmord oder Selbst-Verletzung.
-   Vorsätzliche Einschüchterung.
-   Stalking oder Verfolgung.
-   Belästigendes Fotografieren oder Aufnehmen, einschließlich des
    Protokollierens von Online-Aktivitäten zum Zweck der Belästigung.
-   Andauernde Unterbrechung einer Diskussion oder Unterhaltung.
-   Unerwünschte sexuelle Aufmerksamkeit.
-   Fortwährender, unangebrachter sozialer Kontakt, z.B. das
    Erbitten/Voraussetzen eines unangebrachten Grads der Vertrautheit
    mit anderen.
-   Fortgesetzte, direkte Kommunikation nachdem erbeten wurde, dies zu
    unterlassen.
-   Das vorsätzliche „outen“ eines Teils der Identität einer Person
    ohne deren Einverständnis, außer dies ist notwendig um eine
    gefährdete Person vor beabsichtigtem Missbrauch zu schützen.
-   Das Veröffentlichen von nicht-belästigender, privater Korrespondenz.

ACME Labs ist die Sicherheit marginalisierter Personen wichtiger als der
Komfort privilegierter Personen. Der Vorstand und das Response-Team
behalten sich das Recht vor, nicht zu reagieren auf:

-   Beschwerden wg. umgekehrter Diskriminierung, wie z.B. „umgekehrter
    Rassismus“, „umgekehrter Sexismus“ und „Cis-Feindlichkeit“.
-   Beschwerden über gesetzte Grenzen der Kommunikation, z.B. „lass mich
    in Ruhe“, „geh weg“ oder „Ich will das nicht mir dir diskutieren“.
-   Beschwerden über Kommunikation in einem Tonfall, der dir unpassend
    erscheint.
-   Beschwerden über die Kritik an Rassismus, Sexismus, Cis-Sexismus
    oder Beschwerden über Kritik an anderen, unterdrückenden
    Verhaltensweisen oder Ansichten.

#### Melden von Verstößen

Falls du von einem Mitglied oder Gast von ACME Labs belästigt wirst,
bemerkst, dass jemand anderes belästigt wird, oder irgendwelche anderen
Anliegen hast, schicke bitte dem Response Team eine Mail unter
[`vorstand@acmelabs.space`](mailto:vorstand@acmelabs.space) Falls die
Person, die dich belästigt, Teil des
Response Teams ist, wird die betreffende Person sich nicht an der
Bearbeitung deines Falles beteiligen. Wir Antworten so schnell, wie es
uns möglich ist, spätestens aber nach einer Woche.

Dieser Code of Conduct ist für alle Bereiche von ACME Labs gültig, aber
wenn du durch ein Mitglied von ACME Labs außerhalb unserer Räume
belästigt werden solltest, möchten wird auch davon hören. Wir werden
alle ehrlichen Meldungen über Belästigungen durch Mitglieder von ACME
Labs - speziell auch Belästigungen durch Teile des Response-Teams -
ernst nehmen. Dies schließt Belästigung außerhalb unserer Raume und zu
einem beliebigen Zeitpunkt mit ein. Der Vorstand behält sich das Recht
vor, Mitglieder aufgrund von vergangenem Verhalten aus dem Verein
auszuschließen, auch aufgrund von Verhalten außerhalb von ACME Labs und
aufgrund von Verhalten gegenüber Personen außerhalb von ACME Labs.

Um freiwillige Helfer\*innen im Verein vor Missbrauch und Burnout zu
schützen, behalten wir uns das Recht vor, alle Meldungen abzulehnen, von
denen wir glauben, dass sie in böswilliger Absicht erstellt wurden.
Meldungen, die in der Absicht erstellt wurden, berechtigte Kritik zu
unterdrücken, können ohne Antwort gelöscht werden.

Wir respektieren den Wunsch nach Vertraulichkeit, um Opfer von
Missbrauch zu schützen. Es obliegt unserer Entscheidung Personen, über
die wir eine Beschwerde bezüglich Belästigung erhalten haben, öffentlich
zu benennen oder Dritte im Vertrauen vor dieser Person zu warnen, falls
wir glauben, dass dies die Sicherheit für Gäste und Mitglieder von ACME
Labs oder der allgemeinen Öffentlichkeit erhöhen würde. Wir werden Opfer
von Belästigung nicht ohne ihr Einverständnis öffentlich benennen.

#### Konsequenzen

Mitglieder und Gäste, die gebeten werden, belästigendes Verhalten zu
unterlassen, haben dieser Bitte umgehend Folge zu leisten.

Falls ein Mitglied oder Gast andere Mitglieder oder Gäste belästigt,
kann der Vorstand mit jeder als angemessen erachteten Maßnahme darauf
reagieren. Dies beinhaltet den Ausschluss aus dem Verein sowie
Hausverbot für alle Räume und Veranstaltungen von ACME Labs, die
öffentliche Benennung der betreffenden Person gegenüber anderen
Mitgliedern von ACME Labs sowie gegenüber anderen potenziell betroffenen
Personen oder Organisationen.

## Lizenz

Diese Richtlinien sind unter der [CC0 1.0
Universell](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
lizensiert. Sie sind „public domain“, eine Namensnennung oder offene
Lizensierung deiner Version sind nicht erforderlich. Es handelt sich im
wesentlichen um eine Übersetzung der Beispielrichtlinien aus dem [Geek
Feminism
Wiki](http://geekfeminism.wikia.com/wiki/Community_anti-harassment/Policy),
und wurde von Annalee Flower Horne mit Unterstützung von Valerie Aurora,
Alex Skud Bayley, Tim Chevalier, and Mary Gardiner erstellt und von
Jürgen Peters mit Unterstützung von ACME Labs übersetzt und angepasst.

