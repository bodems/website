+++
title = "A Collective Making Everything"
description = "Ein Hackspace in Bielefeld"
+++

{% covid_info(title="Jeden Freitag ab 18:00 Uhr") %}

Jeden Freitag öffnen wir unsere Räumlichkeiten für alle Interessierten. 
Dies ist der ideale Einstiegstag, um uns kennenzulernen.

Dich können tiefgehende Gespräche fesseln, spontane Kochexperimente überraschen, spontane Brettspielsessions binden sowie Bastel- und Hackprojekte erwarten.

Da wir nicht wissen wie voll es wird, erwarten wir von Euch einen aktuellen
COVID-Test. Einen Schnell-Test könnt ihr kostenfrei bei uns vor der Tür
selbst machen.
{% end %}

<!-- TODO(xanderio): next upcoming event -->

{% definition() %}
**Hack** (\[hæk\]; englisch für technischer Kniff): Funktionserweiterung
oder Problemlösung, die ein Ziel auf eine ungewöhnliche Weise erreicht;
oft im Kontext einer Zweckentfremdung.
{% end %}

[![Collage aus unterschiedlichen Fotos, welches die Räumlichkeiten des ACME Labs darstellen. Die erste Kachel zeigt die Front des Gebäudes mit Pride Flagge im Schaufenster. Daneben ist die Lounge zu sehen, mit Sofas, Tischen und dekorierten Wänden. Unten links ist ein Schaufenster mit Poster und einem alten Fernseher. Im Zentrum befindet sich ein Foto einer Sitzecke mit Tisch und Stühlen. Darunter ein Vortragsraum mit 3D Druckern. Daneben ist ein Foto einer Kappsäge und darunter eines von Plüschhaien auf einem Sofa.](collage.jpg)](collage.jpg)

Wir sind ein Hack- bzw. Makespace in Bielefeld und
interessieren uns in erster Linie für Technologie und wie diese unsere
Gesellschaft verändert. Wir wissen, dass Technologie alleine nicht die
Welt retten und auch missbräuchlich genutzt werden kann, sind aber davon
überzeugt, dass wir Werkzeuge bauen können, die dabei helfen, eine
bessere Zukunft zu gestalten. Bei uns ist jede Person willkommen, die
Interesse an unseren Themen hat, unabhängig von den persönlichen
Fähigkeiten.

„Be excellent to each other“ ist einfacher gesagt als getan, und jede*r
versteht da etwas anderes drunter. Wenn du wissen möchtest, was wir
unter einem vernünftigen Miteinander verstehen, dann lies unseren [Code
of Conduct](@/coc.md).
  