+++
title = "Kascha"
[extra]
interests = "Kunst, Irgendwas mit Medien (Inkscape, GIMP, Darktable, Shotcut), Streaming, Kochen, Backen, HTML/CSS, 3D Druck, basteln mit Hardware/Elektro (RPi/ESP)"
learning = "To learn: programmieren über zusammen hacken hinaus"
emoji = "🌘"
pronouns = "sie/ihr"
+++
