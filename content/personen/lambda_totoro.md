+++
title = "λTotoro"
[extra]
interests = "Programmieren (Haskell, Python), TheoInf, Procedural Art, Pen & Paper Rollenspiele"
learning = "Rust, FreeBSD"
emoji = "🦥"
pronouns = "er/they"
+++
