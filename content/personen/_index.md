+++
template = "people-list.html"
title = "Personen"
description = "Übersicht von Personen im ACME-Labs Hackspace"
+++




In unserem Hackspace sind viele Personen aktiv. Hier ist eine kurze Übersicht.


Wenn dich ein Thema interessiert, komm' gerne vorbei und sprich' uns doch einfach an.

