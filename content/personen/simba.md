+++
title = "simba"
[extra]
interests = "Programmieren (Python, C#) insbesondere Floor Plan Generation, Japanisch, Pen & Paper Rollenspiele"
learning = "Elektrobastelei ggf mit Raspi, Static Site Generation mit Zola"
emoji = "🎇"
pronouns = "er/they"
+++
