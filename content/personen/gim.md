+++
title = "gim"
[extra]
interests = "Programmieren (C#, Haskell, C++, Rust), Dinge mit ESP32, ESPHome, Nähen"
learning = "Rust, Python, Nähen"
emoji = "🐙"
pronouns = "sie/they"
+++
