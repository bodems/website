+++
title = "Kontextfreier Freitag"

[extra]
schedule = "Freitags ab 18:00 Uhr"
+++

<div style="text-align: center;">
Jeden Freitag ab 18Uhr.
</div>

Jeden Freitag öffnen wir unsere Räumlichkeiten für alle Interessierten.
Falls du noch nie im ACME Labs warst, ist dies der ideale Einstiegstag, um das Lab kennenzulernen.
Der Kontextfreier Freitag ist ein Offenes Treffen für Besucher*Innen.

Dich können tiefgehende Gespräche fesseln, spontane Kochexperimente überraschen sowie Bastel- und Hackprojekte erwarten.

Der [CoC der ACME Labs](@/coc.md) gilt auch für diese Veranstaltung.